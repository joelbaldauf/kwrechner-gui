#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPainter>
#include <QCloseEvent>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
    , trayIcon(new QSystemTrayIcon(this))
{
    ui->setupUi(this);
    connect(ui->pushButton_kw_berechnen, SIGNAL(clicked()), SLOT(dateCalculate()));
    connect(ui->pushButton_datum_berechnen, SIGNAL(clicked()), SLOT(kwCalculate()));

    //Tray icon menu
    auto menu = this->createMenu();
    this->trayIcon->setContextMenu(menu);

    QDate *dateToday= new QDate( QDate::currentDate() );
    ui->dateEdit_kw_berechnen->setDate(*dateToday);
    dateCalculate();
    datum heute (dateToday->day(), dateToday->month(), dateToday->year());

    ui->spinBox_datum_jahr->setValue(heute.getJahr());
    ui->spinBox_datum_kw->setValue(heute.kwBerechnen());
    kwCalculate();

    QPixmap pixmap(24,24);
    pixmap.fill(Qt::white);
    QPainter painter(&pixmap);
    painter.drawText(pixmap.rect(),Qt::AlignCenter,QString::number(heute.kwBerechnen()));

    // App icon
    auto appIcon = QIcon(pixmap);
    this->trayIcon->setIcon(appIcon);
    this->setWindowIcon(appIcon);

    // Displaying the tray icon
    this->trayIcon->show();

    // Interaction
    connect(trayIcon, &QSystemTrayIcon::activated, this, &MainWindow::iconActivated);
}

QMenu* MainWindow::createMenu()
{
  // App can exit via Quit menu
  auto quitAction = new QAction("&Quit", this);
  connect(quitAction, &QAction::triggered, qApp, &QCoreApplication::quit);

  auto menu = new QMenu(this);
  menu->addAction(quitAction);

  return menu;
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::iconActivated(QSystemTrayIcon::ActivationReason reason_)
{
  switch (reason_) {
  case QSystemTrayIcon::Trigger:
    if (isHidden())
      show();
    else
        hide();
    break;
  default:
    ;
  }
}

void MainWindow::dateCalculate()
{
    QDate date1 = ui->dateEdit_kw_berechnen->date();

    int day = date1.day();
    int month = date1.month();
    int year = date1.year();
    datum datum1 (day, month, year);
    ui->lineEdit_kw_berechnen_ausgabe->setText(QString::number(datum1.kwBerechnen()));
}

void MainWindow::kwCalculate()
{
    int kw = ui->spinBox_datum_kw->value();
    int jahr = ui->spinBox_datum_jahr->value();

    if (kw == 53 && datum(3,1,jahr+1).kwBerechnen() == 1) {
        kw = 1;
        ui->spinBox_datum_kw->setValue(kw);
        jahr += 1;
        ui->spinBox_datum_jahr->setValue(jahr);
    }
    datum start = datum::kwStartBerechnen(kw, jahr);
    datum ende = datum::kwEndeBerechnen(start);

    QString ausgabe = QString::number(start.getTag()) + "." + QString::number(start.getMonat()) + "." + QString::number(start.getJahr())
            + " - " + QString::number(ende.getTag()) + "." + QString::number(ende.getMonat()) + "." + QString::number(ende.getJahr());
    ui->lineEdit_datum_berechnen_ausgabe->setText(ausgabe);
}


void MainWindow::closeEvent(QCloseEvent *event)
{
    hide();
    event->ignore();
}

int datum::getTag()
{
    return tag;
}

int datum::getMonat()
{
    return monat;
}

int datum::getJahr()
{
    return jahr;
}

bool datum::schaltjahrBestimmen(int jahr) {
    bool sjahr = 0;
    if (!((jahr % 4) || !(jahr % 100)) || !((jahr % 4) || (jahr % 400)))
        sjahr = 1;
    return sjahr;
}

datum::datum() {
    tag = 1;
    monat = 1;
    jahr = 2020;
    schaltjahr = schaltjahrBestimmen(jahr);
}

datum::datum(int t, int m, int j) {

    tag = t;
    monat = m;
    jahr = j;
    schaltjahr = schaltjahrBestimmen(j);

}

datum::~datum() {

}

void datum::ausgeben() {
    cout << tag << " tag " << monat << " Monat " << jahr << " Jahr " << schaltjahr << endl;
}

int datum::wochentag()
{
    int year = jahr;
    int m = monat;
    if (m < 3)
        year--;

    string yearst = to_string(year);
    string cst = yearst.substr(0, 2);
    string yst = yearst.substr(2, 4);
    int y = stoi(yst);
    int c = stoi(cst);
    int d = tag;


    if (m == 1 || m == 2)
        m += 10;
    else
        m -= 2;

    int w = (d + (int)(2.6 * m - 0.2) + y + (int)(y / 4) + (int)(c / 4) - 2 * c) % 7;
    if (w < 0)
        w += 7;

    return w;
}

int datum::kwBerechnen()
{
    int monate[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    int tage = 0;

    if (monat == 1)
        tage = tag;
    else
    {
        for (int i = 0; i < monat - 1; i++)
            tage += monate[i];
        tage += tag;
    }

    if (schaltjahrBestimmen(jahr) == 1 && monat > 2)
        tage += 1;


    datum ersteJan(1, 1, jahr);
    int wochentagErste = ersteJan.wochentag();
    int offset = 0;
    int kw = 0;

    if (wochentagErste <= 4) {
        offset = wochentagErste - 2;
        kw += 1;
    }

    else {
        offset = (8 - wochentagErste) % 7;
        offset = 7 - (offset+1);
    }


    kw += ((tage + offset) / 7);

    if (kw == 0)
    {
        datum datum2(31, 12, jahr - 1);
        if (datum2.kwBerechnen() == 52)
            kw = 52;
        else
            kw = 53;
    }

    else if (kw == 53)
    {
        datum datum1(1, 1, jahr + 1);
        if (datum1.wochentag() < 4)
            kw = 1;
    }

    return kw;
}

void datum::changeMonat(int m)
{
    monat = (monat+m)%12;
    if (monat == 0)
        monat = 12;
}

void datum::changeTag(int t)
{
    tag += t;
}

void datum::changeJahr(int j)
{
    jahr += j;
}

datum datum::kwStartBerechnen(int kw, int jahr)
{
    datum datumErste(1, 1, jahr);

    int kwAnfang = datumErste.kwBerechnen();
    if (kwAnfang == 53 || kwAnfang == 52)
    {
        datumErste.changeTag(3);
        kwAnfang = 1;
    }

    int wochentag = datumErste.wochentag();

    for (int i = 1; i < wochentag; i++)
    {
        datumErste.changeTag(-1);
    }

    int monate[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };

    if (datumErste.schaltjahrBestimmen(jahr))
        monate[1] = 29;

    int stelle = 0;
    for (int i = 1; i < kw; i++)
    {

        datumErste.changeTag(7);
        if (datumErste.getTag() > monate[stelle])
        {
            datumErste.changeTag(-(monate[stelle]));
            datumErste.changeMonat(1);
            stelle++;
        }
    }

    if (datumErste.getTag()<1)
        datumErste = kwStartBerechnen(53, jahr-1);

    else if (datumErste.wochentag()==0)
        datumErste.changeTag(1);

    return datumErste;
}

datum datum::kwEndeBerechnen(datum start)
{
    int monate[12] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
    int vorMonat = start.getMonat();
    if (start.schaltjahrBestimmen(start.getJahr()))
        monate[1] = 29;

    start.changeTag(6);
    if (start.getTag()>monate[start.getMonat()-1]) {
        start.changeTag(-(monate[start.getMonat()-1]));
        start.changeMonat(1);
        if (vorMonat==12 && start.getMonat() == 1)
            start.changeJahr(1);
    }
        return start;
}
