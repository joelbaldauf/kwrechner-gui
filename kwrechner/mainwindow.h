#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <iostream>
#include <string>
#include <QSystemTrayIcon>
#include <QMenu>
using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();


private:
    Ui::MainWindow *ui;
    QSystemTrayIcon* trayIcon;
    QMenu* trayIconMenu;
    QMenu* createMenu();
    void closeEvent(QCloseEvent *bar);

private slots:
     void dateCalculate();
     void kwCalculate();

public slots:
     void iconActivated(QSystemTrayIcon::ActivationReason);
};
#endif // MAINWINDOW_H

class datum {
private:
    int tag;
    int monat;
    int jahr;
    bool schaltjahr;
public:
    int getTag();
    int getMonat();
    int getJahr();
    int wochentag();
    //void setJahr(int);
    //void setMonat(int);
    //void setJahr(int);
    datum(int, int, int);
    datum();
    ~datum();
    bool schaltjahrBestimmen(int);
    int kwBerechnen();
    static datum kwStartBerechnen(int, int);
    static datum kwEndeBerechnen(datum);
    void changeTag(int);
    void changeMonat(int);
    void changeJahr(int);
    void ausgeben();
};
